"""Example script to generate a call to connect a remote channel to an IVR"""
from starpy import manager
from twisted.internet import reactor
import sys, logging
import ipdb

def main( channel = 'sip/555@localhost', connectTo=('devices','555','1') ):
    f = manager.AMIFactory(sys.argv[1], sys.argv[2])
    df = f.login()

    def onLogin( protocol ):
        """On Login, attempt to originate the call"""
        context, extension, priority = connectTo
        df = protocol.originate(
            channel,
            context,extension,priority,
            callerid='myphone',
            async=True,
        )
        # import ipdb
        # ipdb.set_trace()
        def onFinished( result ):
            print '111!!!!!!!', result
            df = protocol.hangup('SIP/localhost-00000010')
            def onHangup( result ):
                print '|\__/|', result
                reactor.stop()
            return df.addCallbacks( onHangup, onHangup )

        def onFailure( reason ):
            ipdb.set_trace()
            print reason.getTraceback()
            return reason
        df.addErrback( onFailure )
        df.addCallbacks( onFinished, onFinished )
        return df

    def onFailure( reason ):
        """Unable to log in!"""
        print reason.getTraceback()
        reactor.stop()
    df.addCallbacks( onLogin, onFailure )
    return df


credentials = {
    'username': 'monast_user',
    'secret': 'qwerty123',
}

originate = dict(
    extension = '555',
    context = 'devices',
    priority = '1',
    channel = 'sip/555@localhost',
    async = True,
)



if __name__ == "__main__":
    manager.log.setLevel( logging.DEBUG )
    logging.basicConfig()
    reactor.callWhenRunning( main )
    reactor.run()
