#! /usr/bin/env python
"""Sample application to watch for PRI exhaustion

This script watches for events on the AMI interface, tracking the identity of
open channels in order to track how many channels are being used.  This would
be used to send messages to an administrator when network capacity is being
approached.

Similarly, you could watch for spare capacity on the network and use that
to decide whether to allow low-priority calls, such as peering framework or
free-world-dialup calls to go through.
"""
from twisted.internet import reactor
import utilapplication
import logging
from basicproperty import common, propertied

log = logging.getLogger( 'priexhaustion' )
log.setLevel( logging.INFO )

import ipdb

POOL_MAX_SIZE = 10
CHANNELS_REFRESH_INTERVAL = 15

import random

from twisted.internet.defer import inlineCallbacks

from starpy import fastagi, manager

class ChannelTracker( propertied.Propertied ):
	"""Track open channels on the Asterisk server"""
	channels = common.DictionaryProperty(
		"channels", """Set of open channels on the system""",
	)
	thresholdCount = common.IntegerProperty(
		"thresholdCount", """Storage of threshold below which we don't warn user""",
		defaultValue = 20,
	)

	def main( self ):
		"""Main operation for the channel-tracking demo"""
		amiDF = APPLICATION.amiSpecifier.login().addCallback( self.onAMIConnect )
		# XXX do something useful on failure to login...

	@inlineCallbacks
	def loginAgent(self, ami):
		# ami = {
		# 	'username': 'monast_user',
		# 	'secret': 'qwerty123',
		# }
		# login = {
		# 	'server': '0.0.0.0',
		# 	'port': 5038,
		# 	'timeout': 5,
		# }
		# mgr = manager.AMIFactory(**ami)
		# msg = yield mgr.login(**login)
		# print msg

		originate = dict(
			exten = '999',
			context = 'agents',
			priority = '1',
			channel = 'sip/999@127.0.0.1',
		)
		res = yield ami.originate(**originate)
		print '---------> ', res
	

	@inlineCallbacks
	def _doOriginate(self, ami):
		originate = dict(
			exten = '555',
			context = 'devices',
			priority = '1',
			channel = 'sip/555@127.0.0.1',
			async = 'True',
		)
		res = yield ami.originate(**originate)
		print res
		# t = random.randint(1, 5)
		# def onSuccess(result):
		# 	print 'originate: %s' % result
		# df.addCallback(onSuccess)

	def scheduleOriginates(self, ami):
		num = random.randint(1, POOL_MAX_SIZE)
		for i in range(num):
			self._doOriginate(ami)

	def onAMIConnect( self, ami ):
		"""Register for AMI events"""
		# XXX should do an initial query to populate channels...
		# XXX should handle asterisk reboots (at the moment the AMI
		# interface will just stop generating events), not a practical
		# problem at the moment, but should have a periodic check to be sure
		# the interface is still up, and if not, should close and restart
		log.debug('onAMIConnect')
		# ami.status().addCallback(self.onStatus, ami=ami)

		ami.registerEvent('Newchannel', self.onChannelNew)

		self.loginAgent(ami)
		self.scheduleOriginates(ami)
		refresh_channels = lambda: self.refreshChannels(ami)
		reactor.callLater(5, refresh_channels)


	def refreshChannels(self, ami):
		with ipdb.launch_ipdb_on_exception():
			luck = random.randint(1, POOL_MAX_SIZE)
			delta = luck - len(self.channels)

			@inlineCallbacks
			def hangup(channel):
				msg = yield ami.hangup(channel=channel)
				print '! channel: ', msg

			if delta < 0:
				delta = min(-delta, len(self.channels))
				for i in range(delta):
					channel = random.choice(self.channels.keys())
					del self.channels[channel]
					print '! channel = ', channel
					hangup(channel)
					
			else:
				for i in range(delta):
					self._doOriginate(ami)
			refresh_time = random.randint(1, CHANNELS_REFRESH_INTERVAL)
			refresh_channels = lambda: self.refreshChannels(ami)
			reactor.callLater(refresh_time, refresh_channels)
			
	def onChannelNew( self, ami, event ):
		"""Handle creation of a new channel"""
		log.debug( """Start on channel %s""", event )

		if 'channel' not in event or'uniqueid' not in event:
			return
		channel = event['channel']
		typ, _, _ = channel.partition('/')
		if typ != 'SIP':
			return
		self.channels[channel] = channel


APPLICATION = utilapplication.UtilApplication()

if __name__ == "__main__":
	logging.basicConfig()
	#log.setLevel( logging.DEBUG )
	#manager.log.setLevel( logging.DEBUG )
	#fastagi.log.setLevel( logging.DEBUG )
	tracker = ChannelTracker()
	reactor.callWhenRunning( tracker.main )
	reactor.run()

'''
Todo:

hangup after hangup
1) agent login




'''
