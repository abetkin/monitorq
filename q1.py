#! /usr/bin/env python
"""Sample application to watch for PRI exhaustion

This script watches for events on the AMI interface, tracking the identity of
open channels in order to track how many channels are being used.  This would
be used to send messages to an administrator when network capacity is being
approached.

Similarly, you could watch for spare capacity on the network and use that
to decide whether to allow low-priority calls, such as peering framework or
free-world-dialup calls to go through.
"""
from twisted.internet import reactor
import utilapplication
import logging
from basicproperty import common, propertied

log = logging.getLogger( 'priexhaustion' )
log.setLevel( logging.INFO )

import ipdb

POOL_MAX_SIZE = 10
CHANNELS_REFRESH_INTERVAL = 15

import random

from twisted.internet.defer import inlineCallbacks

from starpy import fastagi, manager

class ChannelTracker( propertied.Propertied ):
	"""Track open channels on the Asterisk server"""
	channels = common.DictionaryProperty(
		"channels", """Set of open channels on the system""",
	)
	thresholdCount = common.IntegerProperty(
		"thresholdCount", """Storage of threshold below which we don't warn user""",
		defaultValue = 20,
	)

	@inlineCallbacks
	def main( self ):
		self.ami = yield APPLICATION.amiSpecifier.login()
		self.onAMIConnect()
		# XXX do something useful on failure to login...

	@inlineCallbacks
	def loginAgent(self, ami):
		originate = dict(
			exten = '999',
			context = 'agents',
			priority = '1',
			channel = 'sip/999@127.0.0.1',
		)
		res = yield ami.originate(**originate)
		print '---------> ', res
	

	def scheduleOriginates(self, ami):
		num = 10

		@inlineCallbacks
		def doOriginate(ami, callerid):
			originate = dict(
				exten = '555',
				context = 'agents',
				priority = '1',
				channel = 'sip/555@127.0.0.1',
				async = 'True',
				callerid = callerid,
				channelid = callerid,
			)
			res = yield ami.originate(**originate)
			print res

		for i in range(num):
			doOriginate(ami, str(i))

	def onAMIConnect(self):
		ami = self.ami
		ami.registerEvent('Newchannel', self.onChannelNew)
		ami.registerEvent('Agentlogin', self.onAgentLogin)
		ami.registerEvent('Agentlogoff', self.onAgentLogout)

		self.doAgentLogin()
		self.scheduleOriginates(ami)
		self.doHangup(ami)

	@inlineCallbacks
	def doAgentLogin(self):
		yield self.loginAgent(self.ami)
		# reactor.callLater(1, self.repeatedAgentLogin)

	'''
	Agentlogin
	Agentlogoff
	'''

	def onAgentLogout(self, ami, event):
		print '!logout!', event
		self.doAgentLogin()



	def onAgentLogin(self, ami, event):
		print '!login!', event
		self.currentChannel = event['channel']
	

	def doHangup(self):
		channel = self.currentChannel

		@inlineCallbacks
		def hangup():
			msg = yield self.ami.hangup(channel=channel)
			self.currentChannel = None
		
		if channel:
			hangup()

		wait = 1
		reactor.callLater(wait, self.doHangup)
	
	def onChannelNew( self, ami, event ):
		"""Handle creation of a new channel"""
		print """Start on channel %s""" % event 

		if 'channel' not in event or'uniqueid' not in event:
			return
		channel = event['channel']
		typ, _, _ = channel.partition('/')
		if typ == 'Agent':
			print typ
			self.currentChannel = channel
		elif typ != 'SIP':
			return
		self.channels[channel] = channel

		# @inlineCallbacks
		# def hangup():
		# 	yield self.ami.hangup(channel=channel)
		#
		# if event['exten'] == '999':
		# 	reactor.callLater(1, hangup)


APPLICATION = utilapplication.UtilApplication()

if __name__ == "__main__":
	logging.basicConfig()
	#log.setLevel( logging.DEBUG )
	#manager.log.setLevel( logging.DEBUG )
	#fastagi.log.setLevel( logging.DEBUG )
	tracker = ChannelTracker()
	reactor.callWhenRunning( tracker.main )
	reactor.run()


