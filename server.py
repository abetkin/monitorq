from twisted.internet.protocol import Protocol

from twisted.protocols.basic import LineReceiver

from subprocess import Popen

class ScriptRunner(LineReceiver):

    ACTIONS = [
        'start', 'stop',
    ]

    def lineReceived(self, line):
        if line not in self.ACTIONS:
            return
        method = getattr(self, line)
        method()

    def start(self, ):
        self.proc = Popen('python q1.py')
        self.transport.write('The call app started')

    def stop(self, ):
        self.proc.kill()
        self.transport.write('The call app killed')
    
    



from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor

class ScriptRunnerFactory(Factory):
    def buildProtocol(self, addr):
        return ScriptRunner()

# 8007 is the port you want to run under. Choose something >1024
endpoint = TCP4ServerEndpoint(reactor, 8007)
endpoint.listen(ScriptRunnerFactory())
reactor.run()